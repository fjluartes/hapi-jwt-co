'use strict';

const mongoose = require('mongoose');

const dataStore = (options) => {
  if (!options)
    throw new Error('Options parameter required');

  mongoose.connect(options.uri, {
    useNewUrlParser: true
  });

  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', () => {
    console.log(`Connected to ${options.uri}`);
  });

  const Schema = mongoose.Schema;

  return {
    models: require('./models')({
      schema: Schema
    })
  };
};

module.exports = dataStore;
