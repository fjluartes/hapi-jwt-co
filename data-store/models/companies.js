'use strict';

const mongoose = require('mongoose');

const companies = (schema) => {
  const CompanySchema = new schema({
    name: {
      required: true,
      type: String
    },
    city: String,
    address: String
  });
  const Company = mongoose.model('Company', CompanySchema);

  return {
    find: async function() {
      try {
        let result = await Company
            .find({}, (err, data) => {
              if (err) throw err;
              return data;
            });
        return result;
      } catch(err) {
        console.log(err);
        return [];
      }
    },

    findById: async function(name) {
      try {
        let result = await Company
            .findOne({ name: name }, (err, data) => {
              if (err) throw err;
              return data;
            });
        return result;
      } catch(err) {
        console.log(err);
        return [];
      }
    },

    create: async function(fields) {
      let name = fields.name;
      try {
        let result = await Company
            .create(fields, (err, data) => {
              if (err) throw err;
              let msg = name + ' entry created';
              console.log(msg);
            });
        return fields;
      } catch(err) {
        console.log(err);
        return [];
      }
    },

    update: async function(fields) {
      console.log(fields);
      try {
        let result = await Company
            .findOneAndUpdate({ name: fields.name }, fields, (err, data) => {
              if (err) throw err;
              return data;
            });
        let company = await Company
            .findOne({ name: fields.name }, (err, data) => {
              if (err) throw err;
              return data;
            });
        return company;
      } catch(err) {
        console.log(err);
        return [];
      }
    },

    delete: async function(name) {
      try {
        let result = await Company
            .findOneAndDelete({ name: name }, (err, data) => {
              if (err) throw err;
              return data;
            });
        let companies = await Company
            .find({}, (err, data) => {
              if (err) throw err;
              return data;
            });
        return companies;
      } catch(err) {
        console.log(err);
        return [];
      }
    }
  };
};

module.exports = companies;
