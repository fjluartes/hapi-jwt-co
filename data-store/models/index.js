'use strict';

const fs = require('fs');

const models = (options) => {
  if (!options)
    throw new Error('Options parameter required');
  let mod = {};

  fs.readdirSync(__dirname + '/')
	.forEach(function(file) {
	  if (file.match(/\.js$/) && file !== 'index.js') {
		let name = file.replace('.js', '');
		mod[name] = require('./' + file)(options.schema);
	  }
	});
  
  return mod;
};

module.exports = models;
