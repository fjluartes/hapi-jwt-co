'use strict';

const companies = (() => {
  const generate500response = function(err) {
    return {
      statusCode: 500,
      error: 'Internal Server Error',
      message: err.message
    };
  };

  const generate404response = function() {
    return {
      statusCode: 404,
      error: 'Not Found',
      message: 'Company not found'
    };
  };

  const generate400response = function() {
    return {
      statusCode: 400,
      error: 'Bad Request',
      message: 'Invalid Request Payload'
    };
  };
  
  return {
    find: async function(request, h) {
      try {
        let result = await this.dataStore.models.companies
            .find();
        return h.response(result).code(200);
      } catch(err) {
        return h.response(generate500response(err)).code(500);
      }
    },

    findById: async function(request, h) {
      let name = request.params.name;
      try {
        let result = await this.dataStore.models.companies
            .findById(name);
        return h.response(result).code(200);
      } catch(err) {
        return h.response(generate500response(err)).code(500);
      }
    },

    create: async function(request, h) {
      let data = request.payload;
      try {
        let result = await this.dataStore.models.companies
            .create(data);
        return h.response(result).code(200);
      } catch(err) {
        return h.response(generate500response(err)).code(500);
      }
    },

    update: async function(request, h) {
      let data = request.payload;
      try {
        let result = await this.dataStore.models.companies
            .update(data);
        return h.response(result).code(200);
      } catch(err) {
        return h.response(generate500response(err)).code(500);
      }
    },
    
    delete: async function(request, h) {
      let name = request.params.name;
      try {
        let result = await this.dataStore.models.companies
            .delete(name);
        return h.response(result).code(200);
      } catch(err) {
        return h.response(generate500response(err)).code(500);
      }
    }
  };
})();

module.exports = companies;
