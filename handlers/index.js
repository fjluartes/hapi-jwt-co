'use strict';

const fs = require('fs');

fs.readdirSync(__dirname + '/')
  .forEach(function(file) {
    if (file.match(/\.js$/) && file !== 'index.js') {
      let name = file.replace('.js', '');
      exports[name] = require('./' + file);
    }
  });
