'use strict';

require('dotenv').config();
const Hapi = require('hapi');
const hapiJwt = require('hapi-auth-jwt2');

const server = Hapi.server({
  host: process.env.SERVER_HOST,
  port: process.env.SERVER_PORT
});

const dataStore = require('./data-store')({
  uri: process.env.DB_URI
});

const validate = async function(decoded) {
  if (decoded.name !== process.env.NAME) {
    return { isValid: false };
  } else {
    return { isValid: true };
  }
};

const init = async () => {
  await server.register([
    {
      plugin: require('hapi-pino'),
      options: {
        prettyPrint: true,
        logEvents: ['response']
      }
    },
    hapiJwt
  ]);

  server.auth.strategy(
    'jwt', 'jwt',
    { key: process.env.KEY,
      validate: validate,
      verifyOptions: { algorithms: ['HS256'] }
    });

  server.auth.default('jwt');

  server.bind({
    dataStore: dataStore
  });

  server.route(require('./routes'));
  
  await server.start();
  console.log(`Server running at ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  process.exit(1);
  console.log(err);
});

init();
