'use strict';

const handlers = require('./handlers');

module.exports = [
  {
    method: 'GET',
    path: '/companies',
    handler: handlers.companies.find
  },
  {
    method: 'GET',
    path: '/companies/{name}',
    handler: handlers.companies.findById
  },
  {
    method: 'POST',
    path: '/companies',
    handler: handlers.companies.create
  },
  {
    method: 'PUT',
    path: '/companies',
    handler: handlers.companies.update
  },
  {
    method: 'DELETE',
    path: '/companies/{name}',
    handler: handlers.companies.delete
  }
];
